class Consul < FPM::Cookery::Recipe
	name 'consul'
	version lookup('version')
	revision lookup('revision')

	description 'Service discovery and configuration made easy.'
	license 'Mozilla Public License'
	vendor 'HashiCorp'
	homepage 'http://consul.io'

	source "https://releases.hashicorp.com/#{name}/#{version}/#{name}_#{version}_linux_#{lookup('download_arch')}.zip"
	sha256 lookup('sha')

	config_files "/etc/#{name}"
	post_install 'post.sh'
	post_uninstall 'post.sh'
	# build_depends 'unzip'

	def build
	end

	def install
		etc(name).install workdir('config.hcl')
		lib('systemd/system').install workdir('systemd.service'), "#{name}.service"
		bin.install 'consul'
	end
end
