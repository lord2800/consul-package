ui = true
server = true
bootstrap = true
enable_syslog = true

addresses {
	http = "{{GetAllInterfaces | include \"type\" \"IPv4\" | join \"address\" \" \"}}"
}

telemetry {
	prometheus_retention_time = "24h"
	disable_hostname = true
}

http_config "response_headers" {
	"Access-Control-Allow-Origin" = "*"
	"Access-Control-Max-Age" = "86400"
	"Access-Control-Allow-Headers" = "Content-Type"
	"Access-Control-Allow-Methods" = "GET, POST, PUT, DELETE, OPTIONS"
}